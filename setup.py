from setuptools import setup, find_packages

setup(
    name='model salesforce',
    version='0.2',
    description='Models for salesforce data',
    packages=find_packages(),
    package_data={'models': ['**/*.m5o', '*.m5o']},
    install_requires=[],
)
